<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Product extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Product_model', 'product');
        $this->methods['index_get']['limit'] = 20;
    }
    public function index_get()
    {
        $id = $this->get('id_product');
        if ($id === null) {
            $products = $this->product->getProduct();
        } else {
            $products = $this->product->getProduct($id);
        }

        if ($products) {
            $this->response([
                'status' => true,
                'data' => $products
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'data' => 'product not found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_delete()
    {
        $id = $this->delete('id_product');

        if ($id === null) {
            $this->response([
                'status' => false,
                'message' => 'provide an id'
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            if ($this->product->deleteProduct($id) > 0 ) {
                //ok
                $this->response([
                    'status' => true,
                    'id_product' => $id,
                    'message' => 'product has been deleted'
                ], REST_Controller::HTTP_OK);
            } else {
                //id not found
                $this->response([
                    'status' => false,
                    'message' => 'id_product not found'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function index_post()
    {
        $data = [
            'name' =>  $this->post('name'),
            'stock' => $this->post('stock'),
            'price' => $this->post('price')
        ];

        if ($this->product->createProduct($data) > 0) {
            $this->response([
                'status' => true,
                'message' => 'new product has been added'
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false,
                'message' => 'failed to add new product'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_put()
    {
        $id = $this->put('id_product');
        $data = [
            'name' =>  $this->put('name'),
            'stock' => $this->put('stock'),
            'price' => $this->put('price')
        ];

        if ($this->product->updateProduct($data, $id) > 0) {
            $this->response([
                'status' => true,
                'message' => 'product has been updated'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'failed to update product'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}